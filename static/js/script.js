document.querySelector('#generate-user').addEventListener('click', randomUsersGenerator);

fetch("static/utils/previewCard.json").then(resp => resp.json()).then(data => {
    let template = createUserCard(data.results[0]);
    let cardElement = document.createElement('div');
    cardElement.innerHTML = template;
    document.querySelector('#preview_card').appendChild(cardElement);
    updateScript();
})

function randomUsersGenerator(){
    let previewsCards = document.querySelector('#flex-card-users').querySelectorAll('div');

    for (let i=0; i < previewsCards.length; i++){
        previewsCards[i].remove();
    }
    let value = document.getElementById('random-user-number').value;

    let randomUserData = `https://randomuser.me/api/?results=${value}`;
    fetch(randomUserData).then(resp => resp.json()).then(data => {
        let authors = data.results;
        for (let i=0; i < authors.length;i++){
            let template = createUserCard(data.results[i]);
            let cardElement = document.createElement('div');
            cardElement.className = 'card';
            cardElement.innerHTML = template;
            document.querySelector('#flex-card-users').appendChild(cardElement);
        }
    })
    updateScript();
}

function updateScript(){
    $(document).ready(function(){
        $('.tabs').tabs();
        });
}

function createUserCard(userData){
    return `
        <div class="card-content">
            <img src="${userData['picture']['large']}" alt="" class="circle responsive-img">
            <p id="user_title">Hi, my name is</p>
            <p id="user_name">${userData['name']['first']} ${userData['name']['last']}</p>
        </div>
        <div class="card-tabs">
            <ul class="tabs tabs-fixed-width">
            <li class="tab"><a href="#test1"><i class="material-icons">perm_contact_calendar</i></a></li>
            <li class="tab"><a href="#test2"><i class="material-icons">place</i></a></li>
            <li class="tab"><a href="#test3"><i class="material-icons">folder_shared</i></a></li>
            </ul>
        </div>
        <div class="card-content grey lighten-4 user-info"">
            <div id="test1" >
                <div class="icon-box"><i class="material-icons">mail</i>${userData['email']}</div>
                <div class="icon-box"><i class="material-icons">call</i>${userData['phone']}</div>
                <div class="icon-box"><i class="material-icons">smartphone</i>${userData['cell']}</div>
            </div>
            <div id="test2">                    
                <div class="icon-box"><i class="material-icons">person_pin_circle</i>${userData['location']['street']}, ${userData['location']['city']}, ${userData['location']['postcode']}</div>
                <div class="icon-box"><i class="material-icons">navigation</i>${userData['location']['coordinates']['latitude']}, ${userData['location']['coordinates']['longitude']}</div>
            </div>
            <div id="test3">                    
                <div class="icon-box"><i class="material-icons">face</i>${userData['login']['username']}</div>
                <div class="icon-box"><i class="material-icons">lock</i>${userData['login']['password']}</div>
                <div class="icon-box"><i class="material-icons">update</i>${userData['registered']['date'].replace('T', " ").replace('Z', " ")}</div>
            </div>
        </div>
    `
}